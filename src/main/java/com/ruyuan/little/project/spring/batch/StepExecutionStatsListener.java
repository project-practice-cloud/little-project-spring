package com.ruyuan.little.project.spring.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:spring实战
 **/
@Component
public class StepExecutionStatsListener extends StepExecutionListenerSupport {
    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    /**
     * 记录batch更新数据条数
     */
    public ExitStatus afterStep(StepExecution stepExecution){
        log.info("write " + stepExecution.getWriteCount() + " items in step: " + stepExecution.getStepName());
        return null;
    }
}
