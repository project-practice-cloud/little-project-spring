package com.ruyuan.little.project.spring.batch;

import com.ruyuan.little.project.spring.dto.Teacher;
import com.ruyuan.little.project.spring.utils.DateUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:spring实战
 **/
@Component("teacherItemProcessor")
public class TeacherItemProcessor implements ItemProcessor<Teacher, Teacher> {
    @Override
    public Teacher process(Teacher teacher) throws Exception {
        String startTime = teacher.getStartTime();
        int betweenDays = (int) DateUtils.betweenOfDay(startTime, LocalDateTime.now());
        teacher.setTeachingDays(betweenDays);
        return teacher;
    }
}
