package com.ruyuan.little.project.spring.batch;

import com.ruyuan.little.project.spring.dto.Teacher;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:spring实战
 **/
public class TeacherRowMapper implements RowMapper<Teacher> {
    @Override
    public Teacher mapRow(ResultSet rs, int rowNum) throws SQLException {
        Teacher teacher = new Teacher();
        teacher.setId(rs.getInt("id"));
        teacher.setTeacherName(rs.getString("teacher_name"));
        teacher.setCourse(rs.getString("course"));
        teacher.setScore(rs.getBigDecimal("score"));
        teacher.setTeachingDays(rs.getInt("teaching_days"));
        teacher.setStatus(rs.getString("status"));
        teacher.setStartTime(rs.getString("start_time"));
        return teacher;
    }
}
